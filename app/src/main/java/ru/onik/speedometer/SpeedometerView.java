package ru.onik.speedometer;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.View;

/**
 * View that shows a speedometer.
 * It lets customize maximum speed, number of speedometer long and short ticks, tick and text color.
 *
 * @author Onik
 */
public class SpeedometerView extends View {
    // Minimum number of speedometer ticks
    public static final int MIN_TICKS_NUMBER = 2;
    // Minimum number of minor speedometer ticks
    public static final int MIN_MINOR_TICKS_NUMBER = 1;

    // Default text size in dp units
    private static final int DEFAULT_TEXT_SIZE = 12;
    // Default speed value
    private static final float DEFAULT_SPEED = 0;
    // Default speed value
    private static final float DEFAULT_MAX_SPEED = 100;
    // Angle range within which all drawing happens
    private static final float ANGLE = 160;
    // Tick length
    private static final float TICK_HEIGHT = 30;
    // Physical units label
    private static final String UNIT_TEXT = "km/h";

    // Default text and tick color
    private int mDefaultColor = Color.rgb(180, 180, 180);
    // Long tick number
    private int mTicks;
    // Short tick number
    private int mMinorTicks;
    // Max speed
    private float mMaxSpeed;
    // Current speed
    private float mSpeed;
    private Paint mTickPaint;
    private Paint mTextPaint;
    private Paint mBackgroundPaint;
    private Paint mNeedlePaint;
    private Paint mMaskPaint;
    // Bitmap mask
    private Bitmap mMaskBitmap;


    /**
     * Construct a SpeedometerView
     * @param context Context
     */
    public SpeedometerView(Context context) {
        super(context);
        init();
    }

    /**
     * Construct a SpeedometerView
     * @param context Context
     * @param attrs Attributes
     */
    public SpeedometerView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();

        TypedArray array = context.getTheme().obtainStyledAttributes(attrs, R.styleable.SpeedometerView, 0, 0);
        try {
            mTickPaint.setColor(
                    array.getColor(R.styleable.SpeedometerView_tickColor, mDefaultColor));
            mTextPaint.setColor(
                    array.getColor(R.styleable.SpeedometerView_textColor, mDefaultColor));
            setTicks(
                    array.getInteger(R.styleable.SpeedometerView_ticks, MIN_TICKS_NUMBER));
            setMinorTicks(
                    array.getInteger(R.styleable.SpeedometerView_minorTicks, MIN_MINOR_TICKS_NUMBER));
            setMaxSpeed(
                    array.getFloat(R.styleable.SpeedometerView_maxSpeed, DEFAULT_MAX_SPEED));
            setSpeed(
                    array.getFloat(R.styleable.SpeedometerView_speed, DEFAULT_SPEED));
        } finally {
            array.recycle();
        }
    }

    /**
     * Get the color being used to drawing ticks.
     * @return Color integer value
     */
    public int getTickColor() {
        return mTickPaint.getColor();
    }

    /**
     * Set a new color value for tick drawing.
     * @param color New color integer value
     */
    public void setTickColor(int color) {
        mTickPaint.setColor(color);
        invalidate();
    }

    /**
     * Get the color being used to drawing text.
     * @return Color integer value
     */
    public int getTextColor() {
        return mTextPaint.getColor();
    }

    /**
     * Set a new color value for text drawing.
     * @param color New color integer value
     */
    public void setTextColor(int color) {
        mTextPaint.setColor(color);
        invalidate();
    }

    /**
     * Get maximum speed.
     * @return Maximum speed value
     */
    public float getMaxSpeed() {
        return mMaxSpeed;
    }

    /**
     * Set maximum speed.
     * @param maxSpeed New speed value to set
     */
    public void setMaxSpeed(float maxSpeed) {
        if (maxSpeed <= 0) throw new IllegalArgumentException("Non-positive max speed is illegal");

        this.mMaxSpeed = maxSpeed;
        invalidate();
    }

    /**
     * Get current speed
     * @return Current speed value
     */
    public float getSpeed() {
        return mSpeed;
    }

    /**
     * Set current speed.
     * @param speed New current speed value to set
     */
    public void setSpeed(float speed) {
        if (speed < 0) throw new IllegalArgumentException("Negative speed is illegal");

        this.mSpeed = speed > mMaxSpeed ? mMaxSpeed : speed;
        invalidate();
    }

    /**
     * Get ticks number.
     * @return Tick number
     */
    public int getTicks() {
        return mTicks;
    }

    /**
     * Set tick number.
     * @param tickNumber Tick number to set
     */
    public void setTicks(int tickNumber) {
        if (tickNumber < MIN_TICKS_NUMBER) throw new IllegalArgumentException("Ticks number cannot less than MIN_TICKS_NUMBER");

        this.mTicks = tickNumber;
        invalidate();
    }

    /**
     * Get minor ticks number.
     * @return Minor tick number
     */
    public int getMinorTicks() {
        return mMinorTicks;
    }

    /**
     * Set minor ticks number.
     * @param minorTickNumber Minor tick number to set
     */
    public void setMinorTicks(int minorTickNumber) {
        if (minorTickNumber < MIN_MINOR_TICKS_NUMBER) throw new IllegalArgumentException("Minor ticks number cannot less than MIN_MINOR_TICKS_NUMBER");
        this.mMinorTicks = minorTickNumber;
        invalidate();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        canvas.drawColor(Color.TRANSPARENT);
        drawBackground(canvas);
        drawTextAndTicks(canvas);
        drawNeedle(canvas);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int widthMode = MeasureSpec.getMode(widthMeasureSpec);
        int widthSize = MeasureSpec.getSize(widthMeasureSpec);
        int heightMode = MeasureSpec.getMode(heightMeasureSpec);
        int heightSize = MeasureSpec.getSize(heightMeasureSpec);

        int width = (widthMode == MeasureSpec.EXACTLY || widthMode == MeasureSpec.AT_MOST) ?
                widthSize : -1;
        int height = (heightMode == MeasureSpec.EXACTLY || heightMode == MeasureSpec.AT_MOST) ?
                heightSize : -1;

        if (height >= 0 && width >= 0) {
            width = Math.min(height, width);
            height = width / 2;
        } else if (width >= 0) {
            height = width / 2;
        } else if (height >= 0) {
            width = height * 2;
        } else {
            width = 0;
            height = 0;
        }

        setMeasuredDimension(width, height);
    }

    /**
     * Draw a speedometer needle.
     * @param canvas Canvas to draw on
     */
    private void drawNeedle(Canvas canvas) {
        RectF oval = getOval(canvas, 1);
        float radius = oval.width() * 0.35f + 10;
        RectF smallOval = getOval(canvas, 0.1f);

        float angle = 10 + (getSpeed() / getMaxSpeed() * ANGLE);
        canvas.drawLine(
                (float) (oval.centerX() + Math.cos((180 - angle) / 180 * Math.PI) * smallOval.width() * 0.3f),
                (float) (oval.centerY() - Math.sin(angle / 180 * Math.PI) * smallOval.width() * 0.3f),
                (float) (oval.centerX() + Math.cos((180 - angle) / 180 * Math.PI) * radius),
                (float) (oval.centerY() - Math.sin(angle / 180 * Math.PI) * radius),
                mNeedlePaint
        );

        canvas.drawArc(smallOval, 180, 180, true, mBackgroundPaint);
    }

    /**
     * Draw text and speedometer ticks.
     * @param canvas Canvas to draw on
     */
    private void drawTextAndTicks(Canvas canvas) {
        RectF oval = getOval(canvas, 1);
        float radius = oval.width() * 0.35f;

        final float tickAngle = ANGLE / (mTicks - 1);
        final float minorTickAngle = tickAngle / (mMinorTicks + 1);
        float currentAngle = 10;

        for (int i = 0; i < mTicks; i++) {
            // Draw long tick
            canvas.drawLine((float) (oval.centerX() + Math.cos((180 - currentAngle) / 180 * Math.PI) * (radius - TICK_HEIGHT / 2)),
                            (float) (oval.centerY() - Math.sin(currentAngle / 180 * Math.PI) * (radius - TICK_HEIGHT / 2)),
                            (float) (oval.centerX() + Math.cos((180 - currentAngle) / 180 * Math.PI) * (radius + TICK_HEIGHT / 2)),
                            (float) (oval.centerY() - Math.sin(currentAngle / 180 * Math.PI) * (radius + TICK_HEIGHT / 2)),
                            mTickPaint);

            // Draw text for the long tick
            canvas.save();
            canvas.rotate(180 + currentAngle, oval.centerX(), oval.centerY());
            float txtX = oval.centerX() + radius + TICK_HEIGHT / 2 + 8;
            float txtY = oval.centerY();
            canvas.rotate(+90, txtX, txtY);
            canvas.drawText(getText(mMaxSpeed * i / (mTicks - 1)), txtX, txtY, mTextPaint);
            canvas.restore();

            // Break if the last long tick' been drawn
            if (i == mTicks - 1) break;

            float angle;
            // Draw short ticks
            for (int j = 1; j <= mMinorTicks; j++) {
                angle = currentAngle + j * minorTickAngle;

                canvas.drawLine((float) (oval.centerX() + Math.cos((180 - angle) / 180 * Math.PI) * radius),
                        (float) (oval.centerY() - Math.sin(angle / 180 * Math.PI) * radius),
                        (float) (oval.centerX() + Math.cos((180 - angle) / 180 * Math.PI) * (radius + TICK_HEIGHT / 2)),
                        (float) (oval.centerY() - Math.sin(angle / 180 * Math.PI) * (radius + TICK_HEIGHT / 2)),
                        mTickPaint);
            }

            currentAngle += tickAngle;
        }

    }

    /**
     * Round a floating point speed value to integer one and converts it to text.
     * @param value Speed value to convert
     * @return String representation of the speed {@code value}
     */
    private String getText(float value) {
        return String.valueOf(Math.round(value));
    }

    /**
     * Get an oval with respect to {@code factor}
     * @param canvas Canvas to scale from
     * @param factor Scale factor
     * @return Oval scaled with respect to {@code factor}
     */
    private RectF getOval(Canvas canvas, float factor) {
        final int canvasWidth = canvas.getWidth() - getPaddingLeft() - getPaddingRight();
        final int canvasHeight = canvas.getHeight() - getPaddingTop() - getPaddingBottom();

        RectF oval = (canvasHeight * 2 >= canvasWidth) ?
                new RectF(0, 0, canvasWidth * factor, canvasWidth * factor) :
                new RectF(0, 0, canvasHeight * 2 * factor, canvasHeight * 2 * factor);
        oval.offset(
                (canvasWidth - oval.width()) / 2 + getPaddingLeft(),
                (canvasHeight * 2 - oval.height()) / 2 + getPaddingTop());

        return oval;
    }

    /**
     * Draw speedometer background.
     * @param canvas Canvas to draw on
     */
    private void drawBackground(Canvas canvas) {
        RectF oval = getOval(canvas, 0.9f);
        canvas.drawArc(oval, 180, 180, true, mBackgroundPaint);

        Bitmap mask = Bitmap.createScaledBitmap(
                        mMaskBitmap,
                        (int) (oval.width() * 1.1),
                        (int) (oval.height() * 1.1) / 2,
                        true);
        canvas.drawBitmap(
                        mask,
                        oval.centerX() - oval.width() * 1.1f / 2,
                        oval.centerY()-oval.width() * 1.1f / 2,
                        mMaskPaint);

        canvas.drawText(UNIT_TEXT, oval.centerX(), oval.centerY() / 1.5f, mTextPaint);
    }

    /**
     * Initialize class members and view properties.
     */
    private void init() {
        if (!isInEditMode()) setLayerType(View.LAYER_TYPE_HARDWARE, null);

        mSpeed      = DEFAULT_SPEED;
        mMaxSpeed   = DEFAULT_MAX_SPEED;
        mTicks      = MIN_TICKS_NUMBER;
        mMinorTicks = MIN_MINOR_TICKS_NUMBER;

        mTextPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mTextPaint.setColor(mDefaultColor);
        mTextPaint.setLinearText(true);
        mTextPaint.setTextAlign(Paint.Align.CENTER);
        mTextPaint.setTextSize(Math.round(DEFAULT_TEXT_SIZE * getResources().getDisplayMetrics().density));

        mTickPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mTickPaint.setColor(mDefaultColor);
        mTickPaint.setStrokeWidth(5.0f);
        mTickPaint.setStyle(Paint.Style.STROKE);

        mBackgroundPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mBackgroundPaint.setColor(Color.rgb(130, 130, 130));
        mBackgroundPaint.setStyle(Paint.Style.FILL);

        mNeedlePaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mNeedlePaint.setColor(Color.argb(200, 255, 0, 0));
        mNeedlePaint.setStrokeWidth(4);
        mNeedlePaint.setStyle(Paint.Style.STROKE);

        mMaskPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mMaskPaint.setDither(true);

        mMaskBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.mask);
        mMaskBitmap = Bitmap.createBitmap(
                                mMaskBitmap,
                                0, 0,
                                mMaskBitmap.getWidth(),
                                mMaskBitmap.getHeight() / 2);
    }
}