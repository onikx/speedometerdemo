package ru.onik.speedometer;

import android.app.Activity;
import android.os.Bundle;

/**
 * Demo Activity demonstrating SpeedometerView in action.
 * @author Onik
 */
public class ActivitySpeedometer extends Activity {
    // Flag indicating if the Activity is in foreground
    private boolean isPaused = true;
    // Needle animation thread
    private Thread mAnimationThread;
    // Reference to SpeedometerView
    private SpeedometerView speedometer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_speedometer);

        speedometer = (SpeedometerView) findViewById(R.id.speedometer);
        speedometer.setMaxSpeed(80);
        speedometer.setTicks(5);
        speedometer.setMinorTicks(4);

        if (speedometer != null) {
            mAnimationThread = new AnimationThread(125);
            mAnimationThread.start();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        onPause(false);
    }

    @Override
    protected void onPause() {
        super.onPause();
        onPause(true);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mAnimationThread != null) mAnimationThread.interrupt();
    }

    /**
     * Set speed of speedometer.
     *
     * The method blocks a calling thread if Activity is in background.
     * @param speed Speed value to be set.
     */
    private synchronized void setSpeed(final float speed) {
        while(isPaused) {
            try {
                wait();
            } catch (InterruptedException e) {}
        }
        if (speedometer != null)
            speedometer.post(new Runnable() {
                @Override
                public void run() {
                    speedometer.setSpeed(speed);
                }
            });
    }

    /**
     * Set a flag indicating if Activity is in foreground/background.
     *
     * Notifies other waiting threads on a new flag state.
     * @param isPaused Boolean to be set to {@code true} when going background, {@code false} otherwise.
     */
    private synchronized void onPause(boolean isPaused) {
        this.isPaused = isPaused;
        notifyAll();
    }

    /**
     * Background thread for moving the speedometer needle.
     */
    private class AnimationThread extends Thread {
        // Sleep time in milliseconds between needle movements
        private final long mSleepMillis;

        /**
         * Construct an AnimationThread
         * @param sleepMillis Sleep time in milliseconds between needle movements.
         */
        public AnimationThread(long sleepMillis) {
            this.mSleepMillis = sleepMillis;
        }

        /**
         * Implements the needle animation on a background thread.
         * Runs indefinitely starting all over again
         * when the current speed reaches a 1%-range of the max speed.
         */
        public void run() {
            // Max speed (for demo purposes we consider it to be constant)
            final float maxSpeed = speedometer.getMaxSpeed();
            // Time constant (for demo purposes we consider it to be constant)
            final float timeConst = 3 / maxSpeed;
            // Current speed
            float speed;

            try {
                while(true) {
                    if ((speed = speedometer.getSpeed()) == 0) {
                        // A delay before the first move
                        Thread.sleep(2000);
                    }

                    if (speed > 0.99 * maxSpeed) {
                        /*
                        Reached a 1%-interval of max speed.
                        Set the current speed to 0 and start the animation over.
                         */
                        setSpeed(0);
                        continue;
                    }

                    setSpeed(speed + timeConst * (maxSpeed - speed));
                    Thread.sleep(mSleepMillis);
                }
            } catch (InterruptedException e) {}
        }
    }
}